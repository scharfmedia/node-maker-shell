# Node Maker Shell
A tool for creating fully isolated Node / NPM environments.
It installs node and dependencies on first run. After that
it opens a isolated shell (bash) where you can work with
Node and NPM commands.
You can also directly execute commands inside the shell without
entering it.

Default Node engine is iojs 1.3.0 but you can switch to regular NodeJS
and also specify exact version you want. Multiple versions and engines
can run besides, without side effects.
Makershell creates a fakehome for every environment.

This project is very similiar to what NVM does. But it provides more
and cleaner separation of node versions and has also root-user support.
If you dont know if you should use the makershell you probably want to
use NVM, see https://github.com/creationix/nvm .

## Install

    git clone git@bitbucket.org:scharfmedia/node-maker-shell.git
    cd node-maker-shell
    ./maker

## Quickstart

    # start isolated environment
    ./maker

    # execute command in environment 1
    ./maker npm install -g gulp

    # execute command in environment 2
    ./maker gulp

## Supported
Currently only x64 operating systems are supported.

    - Windows x64 (not yet, but soon)
    - Linux x64
    - MacOS x64

## Install specific Node engine and version

    # use Node 0.12.0
    NODE_VERSION=0.12.0 NODE_ENGINE=node ./maker

    # use Node 0.10.36
    NODE_VERSION=0.10.36 NODE_ENGINE=node ./maker

    # use IOJS 1.2.0
    NODE_VERSION=1.2.0 NODE_ENGINE=iojs ./maker

    # you can create a alias to have multiple version shortcuts
    # put this into your ~/.bash_profile or ~/.bashrc or ~/.zshrc
    alias maker12='NODE_VERSION=0.12.0 NODE_ENGINE=node /FULL/PATH/TO/maker'
    alias maker10='NODE_VERSION=0.10.36 NODE_ENGINE=node /FULL/PATH/TO/maker'

    # use alias like
    maker12 npm install -g forever
    maker10 npm install -g gulp