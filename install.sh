#!/bin/bash
# package.json file
function package_json() {
    if [ -f `pwd`/package.json ]; then
        # use package.json from current directory
        cat `pwd`/package.json
    else
        # or use the embedded package.json
        read -r -d '' PACKAGE << EOM
{
  "name": "maker",
  "version": "0.1.1",
  "description": "the node maker shell",
  "repository": "git@bitbucket.org:scharfmedia/node-maker-shell.git",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
  }
}
EOM
#"gulp": "^3.8.11"
        echo $PACKAGE
    fi
}

# extract a single value from a json object
# example: VERSION=$(parse_json `cat package.json` version)
function parse_json() {
    echo $1 | sed -e 's/[{}]/''/g' | awk -F=':' -v RS=',' "\$1~/\"$2\"/ {print}" | sed -e "s/\"$2\"://" | tr -d "\n\t" | sed -e 's/\\"/"/g' | sed -e 's/\\\\/\\/g' | sed -e 's/^[ \t]*//g' | sed -e 's/^"//'  -e 's/"$//'
}

# find operating system for constructing download url later
# currently we support only linux, mac and windows the 64bit versions
# TODO: windows support is not working right now because it needs special download handling
function get_os() {
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
        echo "linux"
    elif [[ "$OSTYPE" == "darwin"* ]]; then
        echo "darwin"
    elif [[ "$OSTYPE" == "cygwin" ]]; then
        echo "win"
    elif [[ "$OSTYPE" == "msys" ]]; then
        echo "win"
    elif [[ "$OSTYPE" == "win32" ]]; then
        echo "win"
    #elif [[ "$OSTYPE" == "freebsd"* ]]; then
    #    echo "freebsd"
    else
        echo "Your platform $OSTYPE is not supported. We support Windows, Linux and MacOS with the x64 versions!"
        exit 1
    fi
}

# currently we only support 64bit architectures
# so detection is quite "simple"
function get_arch() {
    echo "x64"
}

function main() {
    local REALHOME=$HOME
    local SHELL="bash"
    local NODE_ENGINE=${NODE_ENGINE:-"iojs"}
    local NODE_VERSION=${NODE_VERSION:-"1.3.0"}
    local NODE_OS=`get_os`
    local NODE_ARCH=`get_arch`
    local IOJS_URL="https://iojs.org/dist/v${NODE_VERSION}/iojs-v${NODE_VERSION}-${NODE_OS}-${NODE_ARCH}.tar.gz"
    local NODE_URL="http://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-${NODE_OS}-${NODE_ARCH}.tar.gz"
    local PACKAGE=`package_json`
    local APP_TITLE=$(parse_json "$PACKAGE" name)
    local APP_VERSION=$(parse_json "$PACKAGE" version)
    local APP_DESCRIPTION=$(parse_json "$PACKAGE" description)
    local APP_HOME=${DIR:-"$REALHOME/.$APP_TITLE/v$APP_VERSION-$NODE_ENGINE-v$NODE_VERSION"}
    local APP_CACHE="$APP_HOME/cache"
    if [[ "$NODE_ENGINE" == "node" ]]; then
        local NODE=$NODE_URL
    else
        local NODE=$IOJS_URL
    fi
    local NODE_CWD_CACHE="`pwd`/node-${NODE_OS}-${NODE_ARCH}-v${NODE_VERSION}.tar.gz"
    local NODE_CACHE="$APP_CACHE/node-${NODE_OS}-${NODE_ARCH}-v${NODE_VERSION}.tar.gz"
    local NODE_HOME="$APP_HOME/node"
    # create fakehome directory so that NPM and other tools put their
    # files there for more isolation
    local FAKEHOME="$APP_HOME/home"

    # ensure app home exists
    if [ ! -d $APP_HOME ]; then
        mkdir -p $APP_HOME/bin
        mkdir -p $FAKEHOME

        # if there is a real ~/.ssh directory we symlink it
        # into our fake home for allowing git to work with our keys
        if [ -d "$REALHOME/.ssh" ]; then
            ln -sf "$REALHOME/.ssh" "$FAKEHOME/.ssh"
        fi
        # same for git file
        if [ -f "$REALHOME/.gitconfig" ]; then
            ln -sf "$REALHOME/.gitconfig" "$FAKEHOME/.gitconfig"
        fi
        # same for docker file
        if [ -f "$REALHOME/.dockercfg" ]; then
            ln -sf "$REALHOME/.dockercfg" "$FAKEHOME/.dockercfg"
        fi
    fi

    # first try cache file from current working directory
    # in case we deliver node binary archive with this installer
    if [ -f $NODE_CWD_CACHE ]; then
        echo "Use Node cache from current directory..."
        mkdir -p $APP_CACHE
        mv $NODE_CWD_CACHE $NODE_CACHE
    fi

    # download
    if [ ! -f $NODE_CACHE ]; then
        echo "Downloading $NODE_ENGINE v${NODE_VERSION}..."
        mkdir -p $APP_CACHE
        curl -s $NODE > $NODE_CACHE
        if [ $? -eq 1 ]; then
            echo "Could not download from $NODE"
            rm -r $APP_HOME
            exit 1
        fi
    fi

    # extract
    if [ ! -f $NODE_HOME/bin/node ]; then
        echo "Extracting $NODE_ENGINE v${NODE_VERSION}..."
        mkdir -p $NODE_HOME
        tar --strip-components=1 -xzf $NODE_CACHE -C $NODE_HOME
        if [ $? -eq 1 ]; then
            echo "Could not extract archive from $NODE"
            rm -r $APP_HOME
            exit 1
        fi
    fi

    # ensure package.json
    if [ ! -f $APP_HOME/app/package.json ]; then
        mkdir -p $APP_HOME/app
        echo "$PACKAGE" > $APP_HOME/app/package.json

        # install chatmaker dependencies
        PATH="${NODE_HOME}/bin:$PATH" \
          HOME="$FAKEHOME" \
          $SHELL -c "cd $APP_HOME/app && echo \"$APP_DESCRIPTION\" > README.md && npm install"
    fi

    # start shell with command
    if [ "$1" != "" ]; then
        PATH="${APP_HOME}/app/node_modules/.bin:${APP_HOME}/bin:${NODE_HOME}/bin:$PATH" \
        HOME="$FAKEHOME" \
          $SHELL -c "$1"
    else
        # start shell
        PATH="${APP_HOME}/app/node_modules/.bin:${APP_HOME}/bin:${NODE_HOME}/bin:$PATH" \
        HOME="$FAKEHOME" \
          PS1="${APP_TITLE}#${APP_VERSION}:\w > " \
          $SHELL
    fi
}

main "$*"
